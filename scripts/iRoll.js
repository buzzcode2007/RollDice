/* iRoll.js
 put roll results onscreen */

function RollOnInterface(muteSound = false, animation = true, times = 1) {
	
	let roll = {};
	
	
	if (animation) {
		if (muteSound == false) {soundEffects.hit.play();};
		app_dice_appearanceToggle(false);
	};
	
	function rollTheDice() {
		for (rolls = 0; rolls < times; rolls++) {
			roll = rollDiceWithResults();
		}
	}
	
	function playSound() {
		if (roll.result) {
			soundEffects.ding.play()
		};
	};
	
	rollTheDice();
	
	function replaceDice() {
		let diceNumber = 0;
		let value_dices = roll.dice;
		let value_dices_current = 0;
		
		
		for (diceNumber = 0; diceNumber < value_dices.length; diceNumber++) {
			value_dices_current = value_dices[diceNumber];
			
			let dice_identifier = ('#dice-' + (diceNumber + 1));
			$(dice_identifier).removeClass();
			$(dice_identifier).addClass('dice scale-transition center mdi');
			
			let dice_className = ('mdi-dice-' + value_dices_current);
			$(dice_identifier).addClass(dice_className);
			
			if ((value_dices_current == 4) || (value_dices_current == 1)) {
				$(dice_identifier).addClass('red-text text-accent-1');
			} else {
				$(dice_identifier).addClass('white-text');
			}
		};
		
	};
	
	function showReward() {
		let results = roll.result;
		
		switch (results) {
			case 1: M.toast({text: '',  html: '<h4>Six fours!</h4>'}); break;
			case 1.1: M.toast({text: '',  html: '<h4>Six ones!</h4>'}); break;
			case 1.2: M.toast({text: '',  html: '<h4>Six of a kind!</h4>'}); break;
			case 1.3: M.toast({text: '',  html: '<h4>Five fours!</h4>'}); break;
			case 1.4: M.toast({text: '',  html: '<h4>Five of a kind!</h4>'}); break;
			case 1.5: M.toast({text: '',  html: '<h4>Four fours!</h4>'}); break;
			case 2: M.toast({text: '',  html: '<h4>Straight!</h4>'}); break;
			case 2.1: M.toast({text: '',  html: '<h4>Three of a kind!</h4>'}); break;
			case 3: M.toast({text: '',  html: '<h4>Four of a kind!</h4>'}); break;
			case 4: M.toast({text: '',  html: '<h4>Three fours!</h4>'}); break;
			case 5: M.toast({text: '',  html: '<h4>Two fours!</h4>'}); break;
			case 6: M.toast({text: '',  html: '<h4>One four!</h4>'}); break;
		}
	};
	
	
	function rollEffect() {
		$('.btn-floating').removeClass('pulse');
		replaceDice();
		
		if (animation) {app_dice_appearanceToggle(true);}
		if (muteSound == false) {playSound();};
	};
	
	setTimeout(function() {
		rollEffect(); showReward();
	}, 750);
	
	return (roll);
}
