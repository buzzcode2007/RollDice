/* app.js
service workers 
*/

var app = {
	"name": "RollDice",
	"description": "Chinese Dice Game",
	"authors": ['buzz-lightsnack-2007'],
	"sourcecode": "https://gitdab.com/buzz-lightsnack-2007/RollDice"
}

if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('scripts/offline.js', { scope: '/' });
}
