# RollDice

an online Chinese dice game | 网上中秋博饼

<!-- https://apps.apple.com/app/zhong-qiu-bo-bing3d/id942435298?ign-mpt=uo%3D4 -->

## Features
- [ ] randomness
- [ ] auto-scoring
- [ ] cross-compatibility

### For Developers
- [ ] portable

## Installation
To use this software, you can use it directly on the web. 

## Help Wanted
To contribute to this project, feel free to fork (and make a pull request) or submit an issue. 
